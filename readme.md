# Worklist Generation Using R Markdown

## Overview

This R Markdown (`.Rmd`) document facilitates the generation of worklists for GC-MS and LC-MS samples. It adds pyridine washes and alkane standards for GC-MS samples and methanol washes and QC samples for LC-MS samples. 
Additionally, it assigns consecutive vial positions for the trays in the GC-MS and LC-MS instruments.

## Usage

To use this R Markdown document, follow these guidelines:

1. **Prepare Sample Dataframe**:
   - Create a single-column Excel file containing the samples in the order of injection.
   - Ensure that the first three entries represent no injection blanks.
   - Name the single column in your Excel file as "trt."

2. **Create Folders in Your Directory (P Drive)**:
   - Create the following folders in your directory within the P drive:
     - "LCMSMS_Neg_20230905"
     - "LCMSMS_Pos_20230905"
     - "nonPolar_splitless_20230905"
     - "Phytohormones_Neg_20230905"
     - "Phytohormones_Pos_20230905"
     - "polar_split_20230905"
     - "polar_splitless_20230905"

3. **Execute the R Markdown Document**:
   - Open the "worklists_20230904.Rmd" document in RStudio.

4. **Review Output**:
   - The R Markdown document will update the Excel file with additional sheets for every worklist generated.

For more detailed information and instructions on using this R Markdown document, refer to the document itself.
